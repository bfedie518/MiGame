# MiGame

MiGame (maɪ geɪm) is a tool for converting MIDI signals to keyboard presses, mouse movements, or joystick inputs.  I made it so I could play Dark Souls with my electronic drum set and expanded from there.  MiGame has a narrow focus compared to other MIDI translation programs.  This means it's not as flexible, but is better and **easier to use** for what it does.

## -------Another Rewrite--------
It's been a while since I've touched this.  Now that I'm back, I'm redoing the GUI part 
again. Integrating the Godot and the Python wasn't going like I had hoped. Since I last
worked on this, I've discovered the Python module 
[CustomTkinter](https://github.com/TomSchimansky/CustomTkinter). I'll be using 
[my own fork](https://github.com/bfedie518/CustomTkinter/tree/improvements) to make the 
GUI.

For now, the Godot part still works as a config generator and the Python can be run from
a console/terminal.  Everything except the 'Planned Functionality' portion of this README
is accurate for the main branch.

---

## Libraries Used
- [pygame](https://www.pygame.org/news) for MIDI input and GUI
- [pynput](https://pypi.org/project/pynput/) for keyboard and mouse control
- [vgamepad](https://pypi.org/project/vgamepad/) for virtual gamepads.

## Current Functionality

- GUI to generate JSON config files
- Run the translator from the command-line/python interpreter
- Bind MIDI input to keyboard keys, gamepad control, or mouse clicks (no mouse movement)
- Can tap, toggle, press, or release keys based on configuration

## Planned Functionality

Check [this board](https://sharing.clickup.com/36716760/b/h/6-182314545-2/84b2a49fcca1b9c) for planned features and their priority levels.


## Why This Over Another Program

- (As of now) no other MIDI translation program has a built-in key toggle feature.
- It's simple to make and edit control configurations
- It was built with a single goal in mind--playing games--and is optimized for that goal.
- No other MIDI translation program outputs to a virtual gamepad.

## Other MIDI Translation Programs

If MiGame doesn't quite suit your needs, feel free to try one of these other programs (I have nothing to do with the development of these programs).

- [MidiKey2Key](https://midikey2key.de/)
- Bome [MIDI Translator Classic](https://www.bome.com/products/mtclassic) or [MIDI Translator Pro](https://www.bome.com/products/miditranslator)

## License

Licensed under the GNU General Public License v3.0.  You are welcome to use, modify, and/or redistribute MiGame for free.  You may not sell MiGame or derivatives thereof.  You **may** use MiGame as a tool in the creation of other monetized content (such as YouTube videos or Twitch streams).
