class_name Translator
extends PanelContainer

enum Device {KEYBOARD, GAMEPAD, MOUSE}
enum Action {PRESS, RELEASE, TAP, TOGGLE}
enum Note {ON, OFF}

const REQUIRED_FIELDS: Array[String] = [
	"name", "status", "note", "device", "key", "action",
	]

signal delete(me)
signal move(me, want_up)
signal changed()
signal capturing_midi()

var is_collapsed := false : set = _set_collapsed
var is_advanced := false : set = _set_advanced
var is_status_updating := false
var is_capturing_midi := false :
	set(value):
		is_capturing_midi = value
		btn_midi_capture.button_pressed = value

@onready var btn_delete: Button = %BtnDelete
@onready var btn_collapse: TextureButton = %BtnCollapse
@onready var box_expanded: Container = %Expanded
@onready var vbox_move_buttons: VBoxContainer = %MoveButtons # Default
@onready var hbox_move_buttons: HBoxContainer = HBoxContainer.new() # To switch to later
@onready var sep_expand: HSeparator = %HSeparator

@onready var lbl_name: Label = %LblName
@onready var txt_name: LineEdit = %TxtName
@onready var lbl_note: Label = %LblNote
@onready var lbl_key: Label = %LblKey
@onready var lbl_action: Label = %LblAction

@onready var btn_advanced: CheckButton = %BtnAdvanced
@onready var lbl_status: Label = %LblStatus
@onready var spn_status: SpinBox = %Status
@onready var lbl_channel: Label = %LblChannel
@onready var spn_channel: SpinBox = %Channel
@onready var lbl_type: Label = %LblType
@onready var opt_type: OptionButton = %Type
@onready var spn_note: SpinBox = %Note

@onready var opt_device: OptionButton = %Device
@onready var opt_key: OptionButton = %Key
@onready var opt_action: OptionButton = %Action

@onready var spn_velocity_min: SpinBox = %VelocityMin
@onready var spn_velocity_max: SpinBox = %VelocityMax

@onready var btn_midi_capture: Button = %BtnCapture

@onready var lbl_joystick: Label = %LblJoystick
@onready var box_joystick_pos: HBoxContainer = %JoystickPos
@onready var spn_joystick_x: SpinBox = %XPosition
@onready var spn_joystick_y: SpinBox = %YPosition
@onready var lbl_tap_delay: Label = %LblDelay
@onready var spn_tap_delay: SpinBox = %TapDelay


func _ready() -> void:
	opt_device.set_item_metadata(Device.KEYBOARD, "KEYBOARD")
	opt_device.set_item_metadata(Device.GAMEPAD, "GAMEPAD")
	opt_device.set_item_metadata(Device.MOUSE, "MOUSE")

	opt_action.set_item_metadata(Action.PRESS, "PRESS")
	opt_action.set_item_metadata(Action.RELEASE, "RELEASE")
	opt_action.set_item_metadata(Action.TAP, "TAP")
	opt_action.set_item_metadata(Action.TOGGLE, "TOGGLE")

	_on_device_item_selected(opt_device.selected)


func serialize() -> Dictionary:
	var data = {
		"is_collapsed": is_collapsed,
		"is_advanced": is_advanced,
		"name": txt_name.text,
		"status": spn_status.value,
		"note": spn_note.value,
		"velocity": {"min": spn_velocity_min.value, "max": spn_velocity_max.value},
		"device": opt_device.get_item_metadata(opt_device.selected),
		"key": opt_key.get_item_metadata(opt_key.selected),
		"action": opt_action.get_item_metadata(opt_action.selected),
		"joystick_pos": {"x": spn_joystick_x.value, "y": spn_joystick_y.value},
		"tap_delay": spn_tap_delay.value,
	}
	return data


func load_data(data: Dictionary) -> void:
	if data.has("is_collapsed"):
		btn_collapse.button_pressed = data.is_collapsed
	if data.has("is_advanced"):
		btn_advanced.button_pressed = data.is_advanced
	txt_name.text = data.name
	spn_status.value = data.status
	spn_note.value = data.note
	if data.has("velocity"):
		if data.velocity.has(["min"]):
			spn_velocity_min.value = data.velocity.min
		if data.velocity.has(["max"]):
			spn_velocity_max.value = data.velocity.max

	for index in opt_device.item_count:
		if opt_device.get_item_metadata(index) == data.device:
			opt_device.select(index)
			break
	_on_device_item_selected(opt_device.selected)

	for index in opt_key.item_count:
		if opt_key.get_item_metadata(index) == data.key:
			opt_key.select(index)
			break
	_on_key_item_selected(opt_key.selected)

	for index in opt_action.item_count:
		if opt_action.get_item_metadata(index) == data.action:
			opt_action.select(index)
			break
	_on_action_item_selected(opt_action.selected)

	if data.has("joystick_pos"):
		if data.joystick_pos.has_all(["x", "y"]):
			spn_joystick_x.value = data.joystick_pos.x
			spn_joystick_y.value = data.joystick_pos.y
	if data.has("tap_delay"):
		spn_tap_delay.value = data.tap_delay


func stop_capturing() -> void:
	btn_midi_capture.button_pressed = false


func _set_advanced(new_value: bool) -> void:
	is_advanced = new_value
	if is_advanced:
		lbl_status.show()
		spn_status.show()
		lbl_channel.hide()
		spn_channel.hide()
		lbl_type.hide()
		opt_type.hide()
		spn_velocity_max.allow_greater = true
		spn_velocity_min.allow_lesser = true

		if opt_device.selected == Device.KEYBOARD:
			for item in DeviceKey.KEYBOARD_ADVANCED.keys():
				opt_key.add_item(DeviceKey.KEYBOARD_ADVANCED[item])
				opt_key.set_item_metadata(opt_key.item_count - 1, item)
	else:
		lbl_status.hide()
		spn_status.hide()
		lbl_channel.show()
		spn_channel.show()
		lbl_type.show()
		opt_type.show()
		spn_velocity_max.allow_greater = false
		spn_velocity_min.allow_lesser = false

		if opt_device.selected == Device.KEYBOARD:
			for i in DeviceKey.KEYBOARD_ADVANCED.size():
				opt_key.remove_item(DeviceKey.KEYBOARD.size())

	emit_signal("changed")


func _set_collapsed(new_value: bool) -> void:
	is_collapsed = new_value
	if is_collapsed:
		_collapse()
	else:
		_expand()

	emit_signal("changed")


func _expand() -> void:
	box_expanded.show()
	sep_expand.show()
	txt_name.show()
	lbl_name.hide()
	hbox_move_buttons.replace_by(vbox_move_buttons)


func _collapse() -> void:
	box_expanded.hide()
	sep_expand.hide()
	txt_name.hide()
	lbl_name.show()
	vbox_move_buttons.replace_by(hbox_move_buttons)


func _on_btn_delete_pressed() -> void:
	emit_signal("delete", self)


func _on_txt_name_text_changed(new_text: String) -> void:
	lbl_name.text = new_text
	emit_signal("changed")


func _on_note_value_changed(value: float) -> void:
	lbl_note.text = "Note: %s" %value
	emit_signal("changed")


func _on_key_item_selected(index: int) -> void:
	lbl_key.text = opt_key.get_item_text(index)
	if opt_key.get_item_metadata(index).contains("joystick"):
		lbl_joystick.show()
		box_joystick_pos.show()
	else:
		lbl_joystick.hide()
		box_joystick_pos.hide()

	emit_signal("changed")


func _on_action_item_selected(index: int) -> void:
	lbl_action.text = opt_action.get_item_text(index)
	if opt_action.get_item_metadata(index) == "TAP":
		lbl_tap_delay.show()
		spn_tap_delay.show()
	else:
		lbl_tap_delay.hide()
		spn_tap_delay.hide()

	emit_signal("changed")


func _on_device_item_selected(index: int) -> void:
	opt_key.clear()
	var items: Dictionary
	match index:
		Device.KEYBOARD:
			items = DeviceKey.KEYBOARD
			if is_advanced:
				items.merge(DeviceKey.KEYBOARD_ADVANCED)
		Device.GAMEPAD:
			items = DeviceKey.GAMEPAD
		Device.MOUSE:
			items = DeviceKey.MOUSE
	for item in items.keys():
		opt_key.add_item(items[item])
		opt_key.set_item_metadata(opt_key.item_count - 1, item)

	_on_key_item_selected(opt_key.selected)


func _on_velocity_min_value_changed(value: float) -> void:
	if not is_advanced and value > spn_velocity_max.value:
		spn_velocity_min.value = spn_velocity_max.value
	emit_signal("changed")


func _on_velocity_max_value_changed(value: float) -> void:
	if not is_advanced and value < spn_velocity_min.value:
		spn_velocity_max.value = spn_velocity_min.value
	emit_signal("changed")


func _on_btn_advanced_toggled(button_pressed: bool) -> void:
	is_advanced = button_pressed


func _on_btn_up_pressed() -> void:
	emit_signal("move", self, true)


func _on_btn_down_pressed() -> void:
	emit_signal("move", self, false)


func _on_status_value_changed(value: float) -> void:
	if not is_status_updating:
		is_status_updating = true

		var status := value - 127 - 16
		if status <= 0:
			opt_type.select(Note.OFF)
			status += 16
		else:
			opt_type.select(Note.ON)
		spn_channel.value = status

		is_status_updating = false
	emit_signal("changed")


func _update_status(value: float) -> void:
	if not is_status_updating:
		is_status_updating = true
		spn_status.value = spn_channel.value + 127 + (16 if opt_type.selected == Note.ON else 0)
		is_status_updating = false
	emit_signal("changed")


func _on_btn_capture_toggled(button_pressed: bool) -> void:
	if button_pressed:
		emit_signal("capturing_midi")
		is_capturing_midi = true
		self_modulate = Color("0000a4")
	else:
		self_modulate = Color("999999")
		is_capturing_midi = false
