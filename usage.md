## Usage


# Installation

Install for translator:
- [Python](https://www.python.org/downloads/) (3.10 minimum guaranteed to work)
- [pygame](https://www.pygame.org/news) library
- [pynput](https://pypi.org/project/pynput/) library
- [vgamepad](https://pypi.org/project/vgamepad/) library

Install for config generator:
- [Godot 4](https://downloads.tuxfamily.org/godotengine/4.0/beta3/)

You can build/export things if you want, but running the GUI project from the Godot editor and main.py from the command line work too.  Further down the line I'll make nice builds that won't require additional installs.


# Config Generator
1. Click "new translator"
2. Set the input information for a MIDI key
    - Use "Advanced Mode" to directly choose the MIDI status number.  Use normal mode to choose the channel and NoteOn/NoteOff
    - In the 'python' folder is a file called 'monitor_MIDI.py'.  Run this with your MIDI device connected to get the status, note, and velocity values of the keys on your MIDI device.
3. Set the output information, filling in the fields that pop up in the third column as necessary
4. Repeat steps 1-3 for each output
5. Save the file

Notes:
- You can set only one input per translator, but you can use the same MIDI input information to run multiple translators.
- Use the arrow buttons on the left of each translator to move them up/down the list
- Use the arrow button to the right of the movement buttons to collapse/expand each translator to make viewing the list easier
- The GUI is still in its earliest stages and will be receiving many improvements
- The GUI generates a JSON file.  You could technically write this yourself without using the GUI (if you really wanted to).


# Running the Translators
1. Run 'main.py' from the command line or a python interpreter
2. Select the config file you made
3. Select your MIDI **input** device
    - The list shows input and output as separate devices.  Select the one that says "input."
    - Currently, you can only read input from one MIDI device at a time.  There's nothing stopping you from running multiple instances of the program though.
    - If you try to connect to a MIDI device that is already connected to another program, MiGame will crash.
4. Enjoy using your MIDI device to control your keyboard/mouse/gamepad :)
